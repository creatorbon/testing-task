import CartParser from "./CartParser";
import * as uuid from "uuid";

let parser;

beforeEach(() => {
  parser = new CartParser();
});

describe("CartParser - unit tests", () => {
  describe("validate", () => {
    it("should not call createError function when contents are correct format", () => {
      const testedContent = `Product name,Price,Quantity
                            Mollis consequat,9.00,2`;

      parser.createError = jest.fn();
      parser.validate(testedContent);

      expect(parser.createError).not.toHaveBeenCalled();
    });

    it("should create header error when you pass content with incorrect header", () => {
      const testedContent = `Product,Price,Quantity
                            Mollis consequat,9.00,2`;
      const errorMessage =
        'Expected header to be named "Product name" but received Product.';

      parser.createError = jest.fn();
      parser.validate(testedContent);

      expect(parser.createError).toHaveBeenCalledTimes(1);
      expect(parser.createError).toHaveBeenCalledWith(
        "header",
        0,
        0,
        errorMessage
      );
    });

    it("should create row error when you pass content with an incorrect number of cells", () => {
      const testedContent = `Product name,Price,Quantity
                            9.00,2`;
      const errorMessage = "Expected row to have 3 cells but received 2.";

      parser.createError = jest.fn();
      parser.validate(testedContent);

      expect(parser.createError).toHaveBeenCalledTimes(1);
      expect(parser.createError).toHaveBeenCalledWith(
        "row",
        1,
        -1,
        errorMessage
      );
    });

    it("should create cell error when you pass content with empty cell", () => {
      const testedContent = `Product name,Price,Quantity
                              ,9.00,2`;
      const errorMessage =
        'Expected cell to be a nonempty string but received "".';

      parser.createError = jest.fn();
      parser.validate(testedContent);

      expect(parser.createError).toHaveBeenCalledTimes(1);
      expect(parser.createError).toHaveBeenCalledWith(
        "cell",
        1,
        0,
        errorMessage
      );
    });

    it("should create cell error when you pass content with a negative number in cell", () => {
      const testedContent = `Product name,Price,Quantity
                            Mollis consequat,-9.00,2`;
      const errorMessage =
        'Expected cell to be a positive number but received "-9.00".';

      parser.createError = jest.fn();
      parser.validate(testedContent);

      expect(parser.createError).toHaveBeenCalledTimes(1);
      expect(parser.createError).toHaveBeenCalledWith(
        "cell",
        1,
        1,
        errorMessage
      );
    });

    it("should return an error, when the content cell value type is not a number", () => {
      const testedContent = (dataType) =>
        `Product name,Price,Quantity 
        Mollis consequat,${dataType},2`;
      const expectedError = (dataType) => [
        {
          column: 1,
          message: `Expected cell to be a positive number but received "${dataType}".`,
          row: 1,
          type: "cell",
        },
      ];
      const validate = parser.validate.bind(parser);

      expect(validate(testedContent("two"))).toEqual(expectedError("two"));
      expect(validate(testedContent(false))).toEqual(expectedError(false));
      expect(validate(testedContent(Symbol))).toEqual(expectedError(Symbol));
      expect(validate(testedContent(undefined))).toEqual(
        expectedError(undefined)
      );
      expect(validate(testedContent(null))).toEqual(expectedError(null));
      expect(validate(testedContent(NaN))).toEqual(expectedError(NaN));
    });
  });

  describe("parse", () => {
    beforeEach(() => {
      parser.readFile = jest.fn();
      console.error = jest.fn();
    });

    it("should create an error when content has an empty line", () => {
      const testedContent = `Product name,Price,Quantity
        
                            Mollis consequat,-9.00,2`;
      parser.readFile.mockReturnValue(testedContent);

      expect(() => parser.parse()).toThrow(Error);
      expect(console.error).toHaveBeenCalledTimes(1);
    });

    it("should return an error when data is not valid", () => {
      const testedContent = `Product name,Price,Quantity
                            ,2`;
      parser.readFile.mockReturnValue(testedContent);

      expect(() => parser.parse()).toThrow("Validation failed!");
      expect(console.error).toHaveBeenCalledTimes(1);
    });

    it("should return an error when CSV file doesn't have data", () => {
      const testedContent = `Product name,Price,Quantity`;

      parser.readFile.mockReturnValue(testedContent);

      const { items } = parser.parse();
      expect(items).toEqual([]);
    });

    it("should return correct total price if contents are valid", () => {
      const testedContent = `Product name,Price,Quantity
                            Mollis consequat,9.00,2
                            Tvoluptatem,10.32,1`;

      parser.readFile.mockReturnValue(testedContent);

      const { total } = parser.parse();
      expect(total).toBeCloseTo(28.32);
    });
  });

  describe("parseLine", () => {
    it("should return a correct data in JSON object", () => {
      const testedContent = `Mollis consequat,9.00,2`;
      const expectedData = {
        id: "1",
        name: "Mollis consequat",
        price: 9,
        quantity: 2,
      };

      uuid.v4 = jest.fn();
      uuid.v4.mockReturnValue("1");

      expect(parser.parseLine(testedContent)).toMatchObject(expectedData);
    });
  });
});

describe("CartParser - integration test", () => {
  it("should parse CSV and return correct data in JSON format", () => {
    const testedContent = `Product name,Price,Quantity
                          Mollis consequat,9.00,2
                          Tvoluptatem,10.32,1`;
    const expectedData = {
      items: [
        {
          id: "1",
          name: "Mollis consequat",
          price: 9,
          quantity: 2,
        },
        {
          id: "2",
          name: "Tvoluptatem",
          price: 10.32,
          quantity: 1,
        },
      ],
      total: 28.32,
    };

    uuid.v4 = jest.fn();
    uuid.v4.mockReturnValueOnce("1").mockReturnValue("2");

    parser.readFile = jest.fn();
    parser.readFile.mockReturnValue(testedContent);

    expect(parser.parse(testedContent)).toEqual(expectedData);
  });
});
